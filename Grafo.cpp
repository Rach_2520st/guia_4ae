#include <fstream>
#include <iostream>
#include "Grafo.h"

using namespace std;
//para usar fork
#include <unistd.h>

//crea el archivo .txt y le agrega el contenido del 
//arbol para generar la imagen

Grafo::Grafo(Nodo *nodo){

	ofstream fp;
	//se abre el archivo .txt
	fp.open("grafo.txt");
	fp << "digraph G {" << endl;
	fp << "node [style=filled fillcolor=yellow];" << endl;
	//llama a recorrerArbol para que se recorra y se genere el grafo
	recorrer_arbol(nodo, fp);
	//se termina de escribir el archivo .txt
	fp << "}" << endl;
	//se cierra el archivo
	fp.close();
	//se genera el grafo
	system("dot -Tpng -ografo.png grafo.txt &");

	cout << "A continuación se mostrará la imagen" << endl;
	system("eog grafo.png &");

}

void Grafo::recorrer_arbol(Nodo *p, ofstream &fp) {
	
	string cadena = "\0";
	
	if (p != NULL) {
		
		if (p->izq != NULL) {
			fp <<  p->numero << "->" << p->izq->numero << ";" << endl;
		} else {
			cadena = to_string(p->numero) + "i";
			fp <<"\"" << cadena << "\"" <<"[shape=point];" << endl;
			fp << p->numero << "->" <<"\"" << cadena << "\"" << ";" << endl;
		}

		if (p->der != NULL) {
			fp << p->numero << "->" << p->der->numero << ";" << endl;
		} else {
			cadena = to_string(p->numero) + "d";
			fp <<"\"" << cadena << "\"" << "[shape=point];" << endl;
			fp << p->numero << "->" <<"\"" << cadena << "\"" << ";" << endl;
		}

	
		recorrer_arbol(p->izq, fp);
		recorrer_arbol(p->der, fp);
	}
}

