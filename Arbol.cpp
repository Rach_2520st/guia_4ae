#include <iostream>
#include <fstream>
#include "Arbol.h"
#include "Grafo.h"
using namespace std;

Arbol::Arbol(){
}		

//esta función permite crear un nuevo nodo.
Nodo* Arbol::crear(int n, Nodo *padre){
	
	Nodo *nodo_reciente; //genera un nuevo nodo llamado nodo_reciente
	
	nodo_reciente = new Nodo();
	nodo_reciente->numero = n;
	nodo_reciente->padre = padre;
	nodo_reciente->izq = NULL;
	nodo_reciente->der = NULL;

	return nodo_reciente;
}

//esta función permite insertar un elemento al árbol (de tipo entero).
void Arbol::insertar_nodo(Nodo *&arbol, int n, Nodo *padre){
	//si el arbol está vacio(NULL)
	if(arbol == NULL){
		Nodo *nodo_reciente = crear(n, padre);
		arbol = nodo_reciente; //almacena el numero, quedando como raíz

	} else { //si no pasa la condición anterior es porque el árbol ya tiene una raíz.

		int valorRaiz = arbol->numero;
		//se obtiene el valor de la raíz
		//las siguientes condicionales revisan que si el elemento es menor
		//a la raiz se inserta en la izquierda
		//pero si el elemento es mayor se inserta a la derecha, generandose un ABB
		if(n < valorRaiz){

			insertar_nodo(arbol->izq, n, arbol);
		}
		else if(n > valorRaiz){

			insertar_nodo(arbol->der, n, arbol);

		}
		//si no se cumple ninguna de las condicionales anteriores, el elemento ya existe
		else{
			cout << "Ingresaste un valor ya existente en el árbol, intentalo nuevamente" << endl;
		}	
	}
}

//función que busca cierto elemento en los nodos
bool Arbol::busqueda (Nodo *arbol, int n){
	if (arbol == NULL)
	{
		//arbol vacío
		return false;
	}else if (arbol->numero == n){
		//si el nodo es igual al elemento que se está buscando entonces es verdadero
		return true;
	}else if (n < arbol->numero){
		return busqueda(arbol->izq, n);
	}else{
		return busqueda(arbol->der, n);
	}
}
//función que permite la busqueda para la eliminación de un nodo
void Arbol::eliminar (Nodo *arbol, int n){
	if(arbol == NULL){
		return;
	}
	//las siguientes condicionales buscan el elemento a eliminar
	//y luego eliminan el nodo
	else if(n < arbol->numero){
		eliminar(arbol->izq, n);
	}
	else if(n > arbol->numero){
		eliminar(arbol->der, n);
	}else{
		eliminar_nodo(arbol);
	}
} 
//función que permite la eliminación de un nodo
void Arbol::eliminar_nodo (Nodo *nodoEliminar){
	//verifica si el nodo es hoja o padre
	//borra nodo con dos subarboles hijos
	if (nodoEliminar->izq && nodoEliminar->der)
	{
		Nodo *menor = min(nodoEliminar->der);
		nodoEliminar->numero = menor->numero;
		eliminar_nodo(menor); //elimina el nodo

	}
	else if(nodoEliminar->izq)
	{
		reemplazar(nodoEliminar, nodoEliminar->izq);
		destruir_nodo(nodoEliminar);

	}
	else if (nodoEliminar->der)
	{
		reemplazar(nodoEliminar, nodoEliminar ->der);
		destruir_nodo(nodoEliminar);
	}
	else{
		reemplazar(nodoEliminar, NULL);
		destruir_nodo(nodoEliminar);
	}
} 
//función que permite determinar el valor minimos del árbol
//esto quiere decir, lo más a la izquierda posible
Nodo* Arbol::min (Nodo *arbol){
	if (arbol == NULL)
	{
		return NULL;
	}
	if(arbol->izq){
		return min(arbol->izq);

	}else{
		return arbol;
	}
}

//reemplaza un nodo por otro
void Arbol::reemplazar (Nodo *arbol, Nodo *nodo_reciente){
	
	if(arbol->padre){
		if (arbol->numero == arbol->padre->izq->numero)
		{
			arbol->padre->izq = nodo_reciente;
		} else if (arbol->numero == arbol-> padre->der->numero)
		{
			arbol->padre->der = nodo_reciente;
		}

	}
	if (nodo_reciente)
	{
		nodo_reciente->padre = arbol->padre;
	}
}
//función que genera la destrucción de un nodo
void Arbol::destruir_nodo (Nodo *nodo){
	//primero anula los nodos hijos
	nodo->izq = NULL;
	nodo->der = NULL;

	//eliminar nodo
	delete nodo;
}
//esta función llama a la clase grafo para generar el archivo punto txt y luego
// el png que muestra el grafo
void Arbol::graficar (Nodo *arbol){
	if (arbol == NULL)
	{
		return;
	}else{
		cout << "El gráfo se está creando" << endl;
		Grafo *grafo = new Grafo(arbol);
	}
}
//función que muestra el arbol ordenado en preorden
void Arbol::preOrden (Nodo *arbol){
	if(arbol == NULL){
		return;
	}else{
		cout << arbol->numero << "-";
		preOrden(arbol->izq);
		preOrden(arbol->der);
	}
}
//función que muestra el arbol ordenado en inorden
void Arbol::inOrden(Nodo *arbol){
	if (arbol == NULL)
	{
		return;
	}else{
		inOrden(arbol->izq);
		cout << arbol->numero << "-";
		inOrden(arbol->der);
	}
}

//función que muestra el arbol ordenado en postOrden

void Arbol::postOrden(Nodo *arbol){
	if(arbol == NULL){
		return;
	}else{
		postOrden(arbol->izq);
		postOrden(arbol->der);
		cout << arbol->numero << "-";
	}
}
