ARBOL BINARIO:

Este programa se encarga de generar un arbol binario por medio del usuario, el usuario tiene diferentes opciones en el menú.

El programa consta de 3 Clases:

Programa: contiene el menú que conecta las demás clases permitiendo la ejecución de las opciones mostradas en el menú.

Árbol: ejecuta diferente métodos que permiten el ordenamiento y creación del árbol.

Grafo: es la clase que genera el archivo .txt para luego generar la imagen del grafo en formato .png

Los métodos de las clases y sus funciones están comentados y haciendo su trabajo en el código.

Para ejecutar el programa se debe descargar el repositorio actual, luego de esto acceder a este mediante la terminal y luego ingresar el comando "make", este permite ejecutar el programa y para abrirlo se debe ingresar mediante la terminal lo siguiente: "./Programa"

Los requisitos de este programa es encontrarse en un ambiente Linux con el paquete graphviz instalado, luego de esto ejecutar las opciones que indica el programa.
