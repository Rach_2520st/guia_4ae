#include <iostream>
#include <stdlib.h>
#include <stdio.h>

#include "Arbol.h"
#include "Grafo.h"
using namespace std;

void menu(Arbol arbolito){
	int opcion;
	int numero;
	Nodo *arbol = NULL;
	do{

		cout << "Bienvenido a tu arbol" << endl;
		cout << "Ingresa una de las opciones de ejecución" << endl;
		cout << "[1] Crear un nodo" << endl;
		cout << "[2] Mostrar grafo" << endl;
		cout << "[3] Mostrar el contenido del árbol en preorden" << endl;
		cout << "[4] Mostrar el contenido del árbol en postorden" << endl;
		cout << "[5] Mostrar el contenido del árbol en inorden" << endl;
		cout << "[6] Eliminar un nodo" << endl;
		cout << "[7] Modificar un nodo" << endl;
		cout << "[8] Salir" << endl;
		cout << "Ingrese la opción que desea ejecutar" << endl;
		cout << "\n"<< endl;
		cin >> opcion;
		switch(opcion){
			case 1:
				if (arbol == NULL)
				{
					cout << "Tu arbol está vacio" << endl;
					cout << "Ingresar raíz" << endl;
					cin >> numero;
					arbolito.insertar_nodo(arbol, numero, NULL);
				} else{
					cout << "Ingresa un número entero:" << endl;
					cin >> numero;
					arbolito.insertar_nodo(arbol, numero, NULL);
				}
				cout << "/n";
				break;

			case 2:
				cout << "Generando imagen..." << endl;
				if (arbol == NULL)
				{
					cout << "No se puede generar el grafo"<< endl;
					cout << "El grafo está vacio" << endl;

				}else{
					arbolito.graficar(arbol);
				}

				cout << "\n";
				break;
			case 3:
				cout << "Mostrando contenido en preorden" << endl;
				arbolito.preOrden(arbol);
				cout << "\n";
				break;

			case 4:
				cout << "Mostrando contenido en postorden " << endl;
				arbolito.postOrden(arbol);
				cout << "\n" << endl;
				break;


			case 5:
				cout << "Mostrando contenido en Inorden" << endl;
				arbolito.inOrden(arbol);

				break;
			case 6:
				cout << "Ingresa el numero a eliminar:" << endl;
				cin >> numero;
				if (arbolito.busqueda(arbol, numero) == true){
					cout << "Elemento encontrado" << endl;
					arbolito.eliminar(arbol, numero);
					cout << "Elemento eliminado" << endl;
				}else{

					if(arbol == NULL){
						cout << "No hay elemento que eliminar" << endl;
						cout << "Arbol vacio" << endl;

					}
					cout << "Elemento no existe dentro del arbol" << endl;
				}
				cout << "\n" ;
			
				break;
		
			case 7:
				cout << "Ingresa el numero que quieres modificar:" <<endl;
				cin >> numero;
				if(arbolito.busqueda(arbol, numero) == true){
					cout << "Elemento encontrado" << endl;
					//cout << "Elemento modificado" << endl;
				}else{
					if (arbol == NULL)
					{
						cout << "arbol vacio" << endl;
					}
					cout << "eL Elemento no existe, por lo tanto no se puede modificar" << endl;

				}
				cout << "\n" << endl;
				break;
		
			case 8:
				exit(1);
				break;
		
			default: 
				cout << "opcion ingresada no válida" << endl;
				cout << "\n";
				menu(arbolito);
				break;

			}	
	}while (opcion != 8);
}

int main(void)
{
	Arbol arbolito = Arbol();
	menu(arbolito);
	return 0;
}