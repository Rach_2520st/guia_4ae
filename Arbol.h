#include <iostream>

using namespace std;

#ifndef ARBOL_H
#define ARBOL_H

typedef struct _Nodo
{
	int numero;
	struct _Nodo *izq;
	struct _Nodo *der;
	struct _Nodo *padre;
} Nodo;

class Arbol
{
	public:
		//constructor por defecto
		Arbol();

		//metodos del programa
		Nodo *crear (int, Nodo *);
		void insertar_nodo (Nodo *&, int, Nodo *);
		bool busqueda (Nodo *, int);
		void eliminar (Nodo *, int); //busca el elemento a eliminar
		void eliminar_nodo (Nodo *); //elimina el elemento encontrado
		Nodo *min (Nodo *); //minimo
		void reemplazar (Nodo*, Nodo *); //para eliminar nodo con un solo hijo
		void destruir_nodo (Nodo *); //destruye un nodo eliminando tambien a sus hijos
		void graficar (Nodo *); //crea el archivo que contiene el grafo

		//Ordena el arbol en diferentes ordenes
		
		void preOrden (Nodo *);
		void inOrden (Nodo *);
		void postOrden (Nodo *);
};
#endif

